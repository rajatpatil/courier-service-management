
DROP DATABASE IF EXISTS project; 
create database project;
USE project;
DROP TABLE IF EXISTS products;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS customers;
DROP TABLE IF EXISTS staff;
DROP TABLE IF EXISTS order_details;
DROP TABLE IF EXISTS feedback;



create table products (
    productId int primary key auto_increment,
    productName varchar(50),productRate float
    );



create table customers (
    customerId int primary key auto_increment,
    password varchar(100),customerName varchar(50),
    customerPhone varchar(50),customerEmail varchar(50));


create table staff (
    staffId int primary key auto_increment,
    password varchar(100),
    staffName varchar(50),
    staffPhone varchar(15),
    staffEmail varchar(50));



CREATE TABLE feedback (
    id int NOT NULL AUTO_INCREMENT,
    customerId int,
    customerName varchar(50) ,
    customerEmail varchar(50),
    comment varchar(1500),
    status varchar(50),
    PRIMARY KEY (id)
    );
    

create table order_details (
    orderId int primary key auto_increment,
    customerId int,
    productId int,
    staffId int,
    orderDate DATE,
    deliveryDate DATE,
    orderStatus int,
    pickupName varchar(50),
    pickupAddress varchar(50),
    pickupCity varchar(50),
    pickupPin varchar(10),
    pickupPhone varchar(15),
    deliveryName varchar(50),
    deliveryAddress varchar(50),
    deliveryCity varchar(50),
    deliveryPin varchar(10),
    deliveryPhone varchar(15));

create table admin (
    adminId int primary key auto_increment,
    password varchar(100),
    adminName varchar(50),
    adminPhone varchar(15),
    adminEmail varchar(50));

