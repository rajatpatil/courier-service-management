const nodemailer = require('nodemailer')
const config = require('./config')

function sendEmail(email, subject, body, callback) {
  const transport = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: config.emailUser,
      pass: config.password
    }
    
  })

  const options = {
    from: config.emailUser,
    to: email,
    subject: subject,
    html: body
  }

  transport.sendMail(options, callback)
  
}

// sendEmail(
//     'dhirajkumarnikam9158@gmail.com',
//     'Welcome to courier services',`
//     <h1>hello</h1>,
//    <div>welcome to Courier Services,</div>
//    <div>you are successfully signed up...</div>

//    <div>best Regards</div>
//    <div>admin</div>
// `
// )

module.exports = {
  sendEmail: sendEmail
}


