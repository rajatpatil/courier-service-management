const express = require('express')
const utils = require('../../utils')
//const utils = require('../../../utils')

const db = require('../../db')
const config = require('../../config')
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const router = express.Router()


// ----------------------------------------------------
// GET
/**
 * @swagger
 *
 * /admin/profile:
 *   get:
 *     description: For getting administrator profile
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: successful message
 */


router.get('/profile', (request, response) => {
  const statement = `select staffName, staffEmail, staffPhone from staff where staffId = ${request.userId}`
  db.query(statement, (error, staffs) => {
    if (error) {
      response.send({status: 'error', error: error})
    } else {
      if (staffs.length == 0) {
        response.send({status: 'error', error: 'staff does not exist'})
      } else {
        const staff = staffs[0]
        response.send(utils.createResult(error, staff))
      }
    }
  })
})

// ----------------------------------------------------




// POST
/**
 * @swagger
 *
 * /admin/signup:
 *   post:
 *     description: For signing up an administrator
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: firstName
 *         description: first name of admin user
 *         in: formData
 *         required: true
 *         type: string
 *       - name: lastName
 *         description: last name of admin user
 *         in: formData
 *         required: true
 *         type: string
 *       - name: email
 *         description: email of admin user used for authentication
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: admin's password.
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */


router.post('/signup', (request, response) => {
  const {name, email, password,phone} = request.body

  const encryptedPassword = crypto.SHA256(password)
  const statement = `insert into staff (staffName, staffEmail, password,staffPhone) values (
    '${name}', '${email}', '${encryptedPassword}','${phone}'
  )`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})
/**
 * @swagger
 *
 * /admin/signin:
 *   post:
 *     description: For signing in an administrator
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         description: email of admin user used for authentication
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: admin's password.
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */



router.post('/signin', (request, response) => {
  const {email, password} = request.body
  const statement = `select staffId, staffName from staff where  staffEmail = '${email}' and password = '${crypto.SHA256(password)}'`
  db.query(statement, (error, staffs) => {
    if (error) {
      response.send({status: 'error', error: error})
    } else {
      if (staffs.length == 0) {
        response.send({status: 'error', error: 'staff does not exist'})
      } else {
        const staff = staffs[0]
        const token = jwt.sign({id: staff['staffId']}, config.secret)
        response.send(utils.createResult(error, {
          Name: staff['name'],
          token: token
        }))
      }
    }
  })
})











module.exports = router