const express = require('express')
const db = require('../db')
const utils = require('../utils')
const crypto = require('crypto-js')
const config = require('../config')
const mailer =require('../mailer')
const uuid = require('uuid')

router = express.Router()



router.post('/', (request, response) => {
    const { customerId, customerName, customerEmail,comment,status} = request.body
    
    const statement = `insert into feedback (customerId, customerName ,customerEmail, comment,status) values 
    ('${customerId}', '${customerName}' ,'${customerEmail}', '${comment}','${status}')`
    
    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            response.send(utils.createSuccess(data))
            
        }
    })
  })




router.post('/', (request, response) => {
    const {orderDate,pickupName,pickupAddress,pickupCity,pickupPin,pickupPhone,
        deliveryName,deliveryAddress,deliveryCity,deliveryPin,deliveryPhone} = request.body
    const statement = `insert into order_details (orderDate,pickupName,pickupAddress,pickupCity,pickupPin,pickupPhone,
        deliveryName,deliveryAddress,deliveryCity,deliveryPin,deliveryPhone) values ('${orderDate}',
            '${pickupName}','${pickupAddress}','${pickupCity}','${pickupPin}','${pickupPhone}',
            '${deliveryName}','${deliveryAddress}','${deliveryCity}','${deliveryPin}','${deliveryPhone}')`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })


  router.get('/:orderId', (request, response) => {

    const {orderId} =  request.params

    // const statement = `select orderDate,deliveryDate,orderStatus,pickupName,pickupAddress,pickupCity,pickupPin,pickupPhone,
    // deliveryName,deliveryAddress,deliveryCity,deliveryPin,deliveryPhone from order_details where orderId = '${orderId}'`
    
const statement = `select orderId,orderDate,deliveryDate,orderStatus,deliveryName,deliveryAddress,deliveryCity,deliveryPin,deliveryPhone from order_details where orderId = '${orderId}'`

 db.query(statement, (error, orders) => {
     if (error) {
         response.send({ status: 'error', error: error })
     } else {
         if (orders.length == 0) {
             // order does not exist
             response.send({ status: 'error', error: 'order does not exist' })
         } else {
             // order exists
             const order = orders[0]
             response.send({ status: 'success', data: order })
         }
     }
 })
})


router.get('/forgot-password/:email', (request, response) => {
    const {email} = request.params
    const statement = `select customerId, customerName, customerPhone from customers where customerEmail = '${email}'`
    db.query(statement, (error, customers) => {
      if (error) {
        response.send(utils.createError(error))
      } else if (customers.length == 0) {
        response.send(utils.createError('customer does not exist'))
      } else {
        const customer = customers[0]
        const otp = utils.generateOTP()
        const body = `Your otp = ${otp}` 
  
        mailer.sendEmail(email, 'Reset your password', body,  (error, info) => {
          response.send(
            utils.createResult(error, {
              otp: otp,
              email: email
            })
          )
        })
      }
    })
  })
  


router.post('/signup', (request, response) => {
    const { customerName, customerPhone, customerEmail, password } = request.body
    // encrypt user's password
    const ecryptedPassword = crypto.SHA256(password)

    const statement = `insert into customers (customerName, customerPhone, customerEmail, password) values 
        ('${customerName}', '${customerPhone}', '${customerEmail}', '${ecryptedPassword}')`

    db.query(statement, (error, data) => {

        mailer.sendEmail(customerEmail, 'Welcome to couriers', config.body,  (error, info) => {
          response.send(utils.createResult(error, data))
        })
    
      })
})


router.post('/signin', (request, response) => {
    const { customerEmail, password } = request.body

    const encryptedPassword = crypto.SHA256(password)
    const statement = `select customerName,customerEmail,customerPhone from customers where customerEmail = '${customerEmail}' and password = '${encryptedPassword}'`
    db.query(statement, (error, customers) => {
        if (error) {
            response.send({ status: 'error', error: error })
        } else {
            if (customers.length == 0) {
                // user does not exist
                response.send({ status: 'error', error: 'customer does not exist' })
            } else {
                // user exists
                const customer = customers[0]
                response.send({
                    status: 'success', data: {
                        customerName: customer['customerName'],
                        customerEmail: customer['customerEmail'],
                        customerPhone: customer['customerPhone'],
                    }
                })
            }
        }
    })
})


router.put('/', (request, response) => {
    const { customerName, customerPhone, customerEmail,password} = request.body
    const encryptedPassword = crypto.SHA256(password)


    const statement = `update customers set customerName = '${customerName}', customerPhone = '${customerPhone}' where customerEmail = '${customerEmail}' and password = '${encryptedPassword}'`
    
    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            response.send(utils.createSuccess(data))
            
        }
    })
  })
  
  router.delete('/', (request, response) => {
   
    const { customerEmail,password} = request.body
    const encryptedPassword = crypto.SHA256(password)

    const statement = `delete from customers where customerEmail = '${customerEmail}' and password = '${encryptedPassword}'`
    
    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            response.send(utils.createSuccess(data))
            
        }
    })
  })
  




module.exports = router