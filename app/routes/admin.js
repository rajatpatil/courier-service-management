const express = require('express')
const db = require('../db')
const config = require('../config')
const utils = require('../utils')
const router = express.Router()


// Follow url in following format with camelCase convetion
//  URL : '(get|create|update|delete|TABLE_NAME|FEILD_NAME)/:( coreId|relaredId)....'


//-----------------------------------------------------------------------------------------------------------------------
//                                          GET                                     
//-----------------------------------------------------------------------------------------------------------------------

//*******************************************customers***************************************************

router.get('/getAllCustomers', (request, response)=>{
    const statement = `select * from customers`

    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})

router.get('/getCustomerById/:id', (request, response)=>{
    const { id } = request.params
    const statement = `select * from customers where customerId = '${id}'`

    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})
//*******************************************products***************************************************

router.get('/getAllProducts', (request, response)=>{
    const statement = `select * from products`
    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error,data))
    })
})
router.get('/getProductByProductId/:id', (request, response)=>{
    const { id } = request.params
    const statement = `select * from products where productId = '${id}'`
    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error,data))
    })
})

//*******************************************order_details**********************************************

router.get('/getAllOrderDetails', (request, response)=>{
    const statement = `select * from order_details`
    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})
router.get('/getOrderDetailsByOrderId/:id', (request, response)=>{
    const { id } = request.params
    const statement = `select * from order_details where orderId = '${id}'`
    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})
router.get('/getOrderDetailsByStaffId/:id', (request, response)=>{
    const { id } = request.params
    const statement = `select * from order_details where staffId = '${id}'`
    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})
//*******************************************feedback****************************************************

router.get('/getAllFeedbacks', (request, response)=>{
    const statement = `select * from feedback`
    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})

router.get('/getFeedbackByCustomerId/:id', (request, response)=>{
    const { id } = request.params
    const statement = `select * from feedback where customerId = '${id}'`

    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})

//*******************************************staff********************************************************

router.get('/getAllStaff', (request, response)=>{
    const statement = `select * from staff`
    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})
router.get('/getStaffById/:id', (request, response)=>{
    const { id } = request.params
    const statement = `select * from staff where staffId = '${id}'`
    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})

//----------------------------------------------------------------------------------------------------------------------------
//                                           POST                                     
//----------------------------------------------------------------------------------------------------------------------------

//*******************************************products***************************************************
router.post('/createProduct', (request, response)=>{
    const { productName, productRate } = request.body
    const statement = `insert into products (productName, productRate) values ('${productName}', '${productRate}')`
    
    db.query(statement, (error,data)=>{
        response.send(utils.createResult(error, data))
    })
})


//--------------------------------------------------------------------------------------------------------------------------
//                                             PUT                                     
//--------------------------------------------------------------------------------------------------------------------------

//*******************************************products***************************************************
router.put('/updateProductName/:id', (request, response)=>{
    const { id } = request.params
    const { productName } = request.body
    const statement = `update products set productName = '${productName}' where productId = '${id}'`

    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})

router.put('/updateProductRate/:id', (request, response)=>{
    const { id } = request.params
    const { productRate } = request.body
    const statement = `update products set productRate = '${productRate}' where productId = '${id}'`

    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})

router.put('/updateProduct/:id', (request, response)=>{
    const { id } = request.params
    const { productName, productRate } = request.body
    const statement = `update products set productRate = '${productRate}', productName = '${productName}' where productId = '${id}'`

    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})
//*******************************************order_details***************************************************

router.put('/updateOrderDetails/:id', (request, response)=>{
    const { id } = request.params
    const { staffId, deliveryDate } = request.body
    const statement = `update order_details set staffId = '${staffId}', deliveryDate = '${deliveryDate}' where orderId = '${id}'`
    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})

router.put('/updateOrderDetailsStaffId/:id', (request, response)=>{
    const { id } = request.params
    const { staffId } = request.body
    const statement = `update order_details set staffId = '${staffId}' where orderId = '${id}'`
    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})

router.put('/updateOrderDetailsDeliveryDate/:id', (request, response)=>{
    const { id } = request.params
    const { deliveryDate } = request.body
    const statement = `update order_details set deliveryDate = '${deliveryDate}' where orderId = '${id}'`
    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})


//----------------------------------------------------------------------------------------------------------------------------
//                                      DELETE                                     
//----------------------------------------------------------------------------------------------------------------------------

//*******************************************customers************************************************

router.delete('/deleteCustomerById/:id', (request, response)=>{
    const { id } = request.params
    const statement = `delete from customers where customerId = '${id}'`

    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})

//*******************************************feedback*************************************************

router.delete('/deleteFeedbackByCustomerId/:id', (request, response)=>{
    const { id } = request.params
    const statement = `delete from feedback where customerId = '${id}'`

    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})

router.delete('/deleteFeedbackByFeedbackId/:id', (request, response)=>{
    const { id } = request.params
    const statement = `delete from feedback where id = '${id}'`

    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})
//*******************************************products*************************************************

router.delete('/deleteProduct/:id', (request, response)=>{
    const { id } = request.params
    const statement = `delete from products where productId = '${id}'`

    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})

//*******************************************order_details*******************************************

router.delete('/deleteOrderDetailsByOrderId/:id', (request, response)=>{
    const { id } = request.params
    const statement = `delete from order_details where orderId = '${id}'`
    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})

router.delete('/deleteOrderDetailsByStaffId/:id', (request, response)=>{
    const { id } = request.params
    const statement = `delete from order_details where staffId = '${id}'`
    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})
//*******************************************staff***************************************************

router.delete('/deleteStaff/:id', (request, response)=>{
    const { id } = request.params
    const statement = `delete from staff where staffId = '${id}'`
    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})





module.exports = router