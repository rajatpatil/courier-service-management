const express = require('express')
const db = require('../db')
const utils = require('../utils')


const router = express.Router()

//-------------------------------------------------------------
//                       GET                                      
//-------------------------------------------------------------

router.get('/all', (request, response)=>{
    const statement = `select * from products`
    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error,data))
    })
})


router.get('/:id', (request, response)=>{
    const { id } = request.params
    const statement = `select * from products where productId = '${id}'`

    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})

//--------------------------------------------------------------



//--------------------------------------------------------------
//                       POST                                        
//--------------------------------------------------------------
router.post('/create', (request, response)=>{
    const { productName, productRate } = request.body
    const statement = `insert into products (productName, productRate) values ('${productName}', '${productRate}')`
    
    db.query(statement, (error,data)=>{
        response.send(utils.createResult(error, data))
    })
})
//--------------------------------------------------------------



//--------------------------------------------------------------
//                         PUT                                  
//--------------------------------------------------------------
router.put('/productName/:id', (request, response)=>{
    const { id } = request.params
    const { productName } = request.body
    const statement = `update products set productName = '${productName}' where productId = '${id}'`

    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})

router.put('/productRate/:id', (request, response)=>{
    const { id } = request.params
    const { productRate } = request.body
    const statement = `update products set productRate = '${productRate}' where productId = '${id}'`

    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})

router.put('/:id', (request, response)=>{
    const { id } = request.params
    const { productName, productRate } = request.body
    const statement = `update products set productRate = '${productRate}', productName = '${productName}' where productId = '${id}'`

    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})

//---------------------------------------------------------------
//                            DELETE                            
//---------------------------------------------------------------
router.delete('/:id', (request, response)=>{
    const { id } = request.params
    const statement = `delete from products where productId = '${id}'`

    db.query(statement, (error, data)=>{
        response.send(utils.createResult(error, data))
    })
})

module.exports = router