import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FeedbackService} from '../feedback.service';

@Component({
  selector: 'app-feedbacklist',
  templateUrl: './feedbacklist.component.html',
  styleUrls: ['./feedbacklist.component.css']
})
export class FeedbacklistComponent implements OnInit {
  feedbacks =[]
  

  constructor(private router: Router,
    private feedbackService: FeedbackService) { }

  ngOnInit(): void {
    this.loadFeedback()

  }

  deleteFeedback(feedback){

    this.feedbackService.removeFeedback(feedback['customerId']).subscribe(response => {

      if (response['status'] == 'success') {
        this.loadFeedback()
       
      console.log("delete")
      }

    })
  }

 updateStatus(feedback){
   this.feedbackService.updateStatus(feedback['customerId'],feedback['status']).subscribe(response =>{
    if (response['status'] == 'success') {
    
      this.loadFeedback()
      
     }

   })
   

 }

  loadFeedback(){
    this.feedbackService.getFeedback().subscribe( response =>{
      if (response['status'] == 'success') {
        this.feedbacks = response['data']
        console.log(this.feedbacks)
      } else {
        console.log(response['error'])
      }


    })


    

  }

}