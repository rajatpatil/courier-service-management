import { AdminService } from './../admin.service';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-editstaff',
  templateUrl: './editstaff.component.html',
  styleUrls: ['./editstaff.component.css']
})
export class EditstaffComponent implements OnInit {
  id = ''
  name = ''
  password = ''
  phone = ''
  email = ''
  staff = null
  constructor(private activatedRoute:ActivatedRoute,
    private adminService:AdminService,
    private router:Router) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.queryParams['id']
    if(this.id){
       this.adminService.getStaffDetails(this.id).subscribe(response => {
        const staffs = response['data']
        if (staffs.length > 0) {
          this.staff = staffs[0]
          this.name = this.staff['staffName']
          this.phone = this.staff['staffPhone']
          this.email = this.staff['staffEmail']
        }
       })
    }
  }
  onUpdate(){
     if (this.staff) {
      this.adminService.updateStaff(this.id,this.name,this.phone,this.email)
      .subscribe(response => {
           if (response['status'] == 'success') {
             this.router.navigate(['/stafflist'])
           }
      })
     }else{
      this.adminService.addStaff(this.password,this.name,this.phone,this.email)
      .subscribe(response => {
           if (response['status'] == 'success') {
             this.router.navigate(['/stafflist'])
           }
      })
     }
  }

}
