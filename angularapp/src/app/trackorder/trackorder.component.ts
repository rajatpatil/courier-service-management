import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrderService } from './../order.service';

@Component({
  selector: 'app-trackorder',
  templateUrl: './trackorder.component.html',
  styleUrls: ['./trackorder.component.css']
})
export class TrackorderComponent implements OnInit {

 
  order=null
  id:number=0

  constructor(private router: Router,private orderService:OrderService) { }
  

  ngOnInit(): void {
    
  }
  

  trackorder(){
   

    this.orderService.getOrderDetails(this.id).subscribe(response =>{
     
        this.order=response[0]
        console.log(this.order)
     

    })
    


  }

}
