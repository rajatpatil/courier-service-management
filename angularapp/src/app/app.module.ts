import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { AdminComponent } from './admin/admin.component';
import { StaffComponent } from './staff/staff.component';
import { CustomerComponent } from './customer/customer.component';
import { TrackorderComponent } from './trackorder/trackorder.component';
import { DeliveryComponent } from './delivery/delivery.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { FeedbacklistComponent } from './feedbacklist/feedbacklist.component';
import { OrderlistComponent } from './orderlist/orderlist.component';
import { OrderService } from './order.service';
import { StafflistComponent } from './stafflist/stafflist.component';
import { AdminService } from './admin.service';
import { EditstaffComponent } from './editstaff/editstaff.component';
import { ProductlistComponent } from './productlist/productlist.component';
import { EditproductComponent } from './editproduct/editproduct.component';
import { StaffemailComponent } from './staffemail/staffemail.component';
import { CustomerlistComponent } from './customerlist/customerlist.component';
import { ProfileComponent } from './profile/profile.component';
import { CustomerprofileComponent } from './customerprofile/customerprofile.component';
import { CompletedordersComponent } from './completedorders/completedorders.component';
import { PendingordersComponent } from './pendingorders/pendingorders.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactComponent } from './contact/contact.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    AdminComponent,
    StaffComponent,
    CustomerComponent,
    TrackorderComponent,
    DeliveryComponent,
    FeedbackComponent,
    FeedbacklistComponent,
    OrderlistComponent,
    StafflistComponent,
    EditstaffComponent,
    ProductlistComponent,
    EditproductComponent,
    StaffemailComponent,
    CustomerlistComponent,
    ProfileComponent,
    CustomerprofileComponent,
    CompletedordersComponent,
    PendingordersComponent,
    AboutusComponent,
    ContactComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  
  ],
  providers: [
    OrderService,
    AdminService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
