import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  constructor(private router:Router) { 
    if(sessionStorage['role']!="customer"){
      alert("You are not a customer")
      this.router.navigate(['/login'])
    }
  }

  ngOnInit(): void {
  }

}
