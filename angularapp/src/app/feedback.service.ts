import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  url=''

  constructor(private router:Router,private httpClient:HttpClient) { }



  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
        // client-side error
        errorMessage = `Error: ${error.error.message}`;
        alert("client side error");
        
    } else {
        // server-side error
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
       
       
    }
    console.log(errorMessage);
    return throwError(errorMessage);
}
  
  
  giveFeedback(cid:number,name:string,email:string,comment:string,status:string){
    this.url ='http://localhost:3000/customer'
    const body = {
      customerId:cid,
      customerName:name,
      customerEmail:email,
      comment:comment,
      status:status
    }
    

    return this.httpClient.post(this.url+'/',body)
  }

  sendMail(name:string,email:string){
    this.url ='http://localhost:5000/feedback'
    


    const body ={
      name:name,
      email:email
    }

  return  this.httpClient.post(this.url+'/sendmail/',body).pipe(retry(1),
  catchError(this.handleError));

  }


  getFeedback(){

    this.url ='http://localhost:5000/feedback'


    return this.httpClient.get(this.url+'/')
  }

  removeFeedback(id:number){
    this.url ='http://localhost:5000/feedback'
    
    
    return this.httpClient.delete(this.url+'/'+id)
 


  }

  updateStatus(id:number,status:string) {
    // add the token in the request header
    this.url ='http://localhost:5000/feedback'

   const body = {
     status:'solved'
    
   }
   
   return this.httpClient.put(this.url + '/updatestatus/' + id, body)
 }
 





}
