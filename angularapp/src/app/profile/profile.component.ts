import { Component, OnInit } from '@angular/core';
import { UserService } from './../user.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';



@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  id=0
  profile=null

  constructor(private router: Router,private userService:UserService) { }

  ngOnInit(): void {
  }

  getProfile(){

    this.userService.getById(this.id).subscribe(response =>{
      this.profile = response[0]
      console.log(this.profile)

    })

  }



}
