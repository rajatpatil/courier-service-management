import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class UserService implements CanActivate {
 
  url=''

  constructor(
    private router: Router,
    private httpClient: HttpClient) { }

    register(name:string,email:string,password:string,phone:string){

      const user =prompt('enter the user type admin/customer/staff')
      
    if(user=='admin')
    {
      this.url ='http://localhost:4000/admin'
    }
    if(user=='staff')
    {
      this.url ='http://localhost:5000/staff'
    }
    if(user=='customer')
    {
      this.url ='http://localhost:3000/customer'
    }
    const body = {
      name:name,
      email: email,
      password: password,
      phone:phone

    }
    return this.httpClient.post(this.url + '/signup', body)

    }

    getCustomerlist(){
      this.url ='http://localhost:5000/staff'
      return this.httpClient.get(this.url+'/customerlist/')


    }
    //staff
    getById(id:number){
    
    
    
    
      this.url ='http://localhost:5000/staff'
   
    

      

      return this.httpClient.get(this.url+'/profile/'+id)
      

    }
    //customer
    getCustomerById(id :number){
      this.url ='http://localhost:3000/customer'
      return this.httpClient.get(this.url+'/profile/'+id)



    }

    handleError(error) {
      let errorMessage = '';
      if (error.error instanceof ErrorEvent) {
          // client-side error
          errorMessage = `Error: ${error.error.message}`;
          alert(errorMessage);
          
      } else {
          // server-side error
          errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
          alert(errorMessage);
         
      }
      console.log(errorMessage);
      return throwError(errorMessage);
  }

  
  login(email: string, password: string) {
    const user =prompt('enter the user type admin/customer/staff')
    sessionStorage['role'] = user
    if(user=='admin')
    {
      this.url ='http://localhost:4000/admin'
    }
    if(user=='staff')
    {
      this.url ='http://localhost:5000/staff'
    }
    if(user=='customer')
    {
      this.url ='http://localhost:3000/customer'
    }

    const body = {
      email: email,
      password: password
    }

    return this.httpClient.post(this.url + '/signin', body)
  }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (sessionStorage['token']) {
      // user is already logged in
      // launch the component
      return true
    }

    // force user to login
    this.router.navigate(['/login'])

    // user has not logged in yet
    // stop launching the component
    return false 
  }

 
}
