import { UserService } from './../user.service';
import { Component, OnInit } from '@angular/core';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email = ''
  password = ''

  constructor(  private router: Router,private userService:UserService) { 

  }

  ngOnInit(): void {

   
  }
  login()
  {
   
    this.userService
    .login(this.email, this.password)
    .subscribe(response => {
      if (response['status'] == 'success') {
        const data = response['data']
        console.log("You have logged in successfully")
        
        // cache the user info
        sessionStorage['token'] = data['token']
        sessionStorage['Name'] = data['Name']
        this.router.navigate(['/home'])

      } else {
        alert('invalid email or password')
      }
    })

  }

}
