import { Component, OnInit } from '@angular/core';
import { OrderService } from '../order.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-orderlist',
  templateUrl: './orderlist.component.html',
  styleUrls: ['./orderlist.component.css']
})
export class OrderlistComponent implements OnInit {
  orders=[]

  constructor(private router: Router,
    private orderService:OrderService) { }

  ngOnInit(): void {
    this.loadOrders()
  }

  deleteOrder(order){
    this.orderService.removeOrder(order['orderId']).subscribe(response =>{
      if (response['status'] == 'success') {
        this.loadOrders()
       
      console.log("delete")
      }

    })

  }
  updateOrderStatus(order){
    this.orderService.updateOrderStatus(order['orderId'],order['orderStatus']).subscribe(response =>{

      if (response['status'] == 'success') {
        this.loadOrders()
       
      console.log("update")
      }

    })

  }


  loadOrders(){

    this.orderService.getAllOrders().subscribe(response =>{
      if (response['status'] == 'success') {
        this.orders = response['data']
        console.log(this.orders)
      } else {
        console.log(response['error'])
      }


    })
  }

}
