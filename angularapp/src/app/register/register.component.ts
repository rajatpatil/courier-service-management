import { Component, OnInit } from '@angular/core';
import { UserService } from './../user.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  name=''
  password=''
  email=''
  phone=''

  constructor( private router: Router,private userService:UserService) { }

  ngOnInit(): void {
  }

  register(){
    this.userService.register(this.name,this.email,this.password,this.phone).subscribe(response=>{

      if (response['status'] == 'success'){

        console.log("You have registered successfully")
        this.router.navigate(['/login'])

      }
      else{
        console.log(response['error'])
      }
    })

  }

}
