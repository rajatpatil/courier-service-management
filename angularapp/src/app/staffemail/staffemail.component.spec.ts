import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffemailComponent } from './staffemail.component';

describe('StaffemailComponent', () => {
  let component: StaffemailComponent;
  let fixture: ComponentFixture<StaffemailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StaffemailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffemailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
