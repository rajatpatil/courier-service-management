import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FeedbackService} from '../feedback.service';

@Component({
  selector: 'app-staffemail',
  templateUrl: './staffemail.component.html',
  styleUrls: ['./staffemail.component.css']
})
export class StaffemailComponent implements OnInit {
  name=""
  email=""

  constructor(private router: Router,
    private feedbackService: FeedbackService) { }

  ngOnInit(): void {
  }

  sendMail(){
    this.feedbackService.sendMail(this.name,this.email).subscribe(response =>{
      if (response['status'] == 'success') {
       
       console.log(response['data'])
       alert("You have sent Email successfully to User")
       this.router.navigate(['/staff'])
     
      }

    })
  }

}