import { Component, OnInit } from '@angular/core';
import { OrderService } from '../order.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-completedorders',
  templateUrl: './completedorders.component.html',
  styleUrls: ['./completedorders.component.css']
})
export class CompletedordersComponent implements OnInit {
  orders =[]
  count =0


  constructor(private router: Router,
    private orderService:OrderService) { }

  ngOnInit(): void {
    this.loadCompletedOrders()
  }


  loadCompletedOrders(){

    this.orderService.getCompletedOrders().subscribe(response =>{


      if (response['status'] == 'success') {
        this.orders = response['data']
        console.log(this.orders)

        this.count = this.orders.length
      } else {
        console.log(response['error'])
      }

    })

  }

}
