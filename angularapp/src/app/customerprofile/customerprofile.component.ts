import { Component, OnInit } from '@angular/core';
import { UserService } from './../user.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customerprofile',
  templateUrl: './customerprofile.component.html',
  styleUrls: ['./customerprofile.component.css']
})
export class CustomerprofileComponent implements OnInit {
  id=0
  profile =null

  constructor(private router: Router,private userService:UserService) { }

  ngOnInit(): void {

  }


  getProfile(){
    this.userService.getCustomerById(this.id).subscribe(response=>{

      this.profile = response[0]
      console.log(this.profile)
    })
  }

}
