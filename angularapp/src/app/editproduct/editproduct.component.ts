import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-editproduct',
  templateUrl: './editproduct.component.html',
  styleUrls: ['./editproduct.component.css']
})
export class EditproductComponent implements OnInit {
  id = ''
  name = ''
  rate = 0
  product = null
  constructor(private activatedRoute:ActivatedRoute,
    private adminService:AdminService,
    private router:Router) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.queryParams['id']
    if(this.id){
       this.adminService.getProductDetails(this.id).subscribe(response => {
        const products = response['data']
        if (products.length > 0) {
          this.product = products[0]
          this.name = this.product['productName']
          this.rate = this.product['productRate']
        }
       })
    }
  }
  onUpdate(){
     if (this.product) {
      this.adminService.updateProduct(this.id,this.name,this.rate)
      .subscribe(response => {
           if (response['status'] == 'success') {
             this.router.navigate(['/productlist'])
           }
      })
     }else{
      this.adminService.addProduct(this.name,this.rate)
      .subscribe(response => {
           if (response['status'] == 'success') {
             this.router.navigate(['/productlist'])
           }
      })
     }
  }
}
