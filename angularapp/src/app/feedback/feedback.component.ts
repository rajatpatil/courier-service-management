import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FeedbackService} from '../feedback.service';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {
comment=""
name=""
email=""
cid=0
status=""
  constructor(private router: Router,
    private feedbackService: FeedbackService) { }

  ngOnInit(): void {
  }
  addFeedback(){
    console.log(this.name)
    this.feedbackService.giveFeedback(this.cid,this.name,this.email,this.comment,this.status).subscribe(response =>{
      if (response['status'] == 'success') {
          console.log("data added")
          this.router.navigate(['/customer'])
       
      }

    }

    )


  }

}
