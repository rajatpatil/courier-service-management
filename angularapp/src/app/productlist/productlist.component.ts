import { Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.css']
})
export class ProductlistComponent implements OnInit {
  allproducts = null
  constructor(private adminService:AdminService,
    private router:Router) { }

  ngOnInit(): void {
    this.getProductList()
  }
  getProductList(){
    this.adminService.getAllProducts().subscribe(response => {
      this.allproducts=response["data"]
      console.log(this.allproducts)
    })
  }

  updateProduct(product){
    this.router.navigate(['/editproduct'], {queryParams: {id: product['productId']}})
  }
  deleteProduct(product){
    var temp = window.confirm("Are you sure ? Delet " + product.staffName)
    if(temp == true)
    this.adminService.deleteProduct(product.productId).subscribe(response => {
      if (response["status"]) {
        window.alert("Deleted")
        this.getProductList()
      }
    })
  }
}
