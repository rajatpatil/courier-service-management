import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class OrderService {
  url=''

  constructor(private router: Router,
    private httpClient: HttpClient) { }
    updateOrderStatus(id:number,status:number){
      this.url ='http://localhost:5000/order'

      const body ={
        status:status
      }

      return this.httpClient.put(this.url+'/updateorderstatus/'+id,body)



    }
    giveOrder(customerId:number,productId:number,orderDate,
      pickupName:string,pickupAddress:string,pickupCity:string,pickupPin:string,pickupPhone:string,
      deliveryName:string,deliveryAddress:string,deliveryCity:string,deliveryPin:string,deliveryPhone:string)
      {
        this.url='http://localhost:3000/customer/add'

        const body = {
          productId: productId,
            orderDate: orderDate,
            pickupName: pickupName,
            pickupAddress: pickupAddress,
            pickupCity:pickupCity,
            pickupPin: pickupPin,
            pickupPhone: pickupPhone,
            deliveryName: deliveryName,
            deliveryAddress: deliveryAddress,
            deliveryCity: deliveryCity,
            deliveryPin: deliveryPin,
            deliveryPhone: deliveryPhone


        }
        return this.httpClient.post(this.url+'/'+customerId,body)



      }


    removeOrder(id:number){
      this.url ='http://localhost:5000/order'

     
      return this.httpClient.delete(this.url+'/removeorder/'+id)


    }

    getAllOrders(){
      this.url ='http://localhost:5000/order'
      return this.httpClient.get(this.url + '/')


    }
    getCompletedOrders(){
      this.url = 'http://localhost:4000/admin'
      return this.httpClient.get(this.url+'/getCompletedOrders/')

    }
    getPendingOrders(){
      this.url = 'http://localhost:4000/admin'
      return this.httpClient.get(this.url+'/getPendingOrders/')

    }

    getOrderDetails(id){
      this.url ='http://localhost:5000/order'
    



      return this.httpClient.get(this.url + '/getorderdetails/'+id)




    }

}
