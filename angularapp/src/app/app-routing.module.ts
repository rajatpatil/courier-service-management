import { ProductlistComponent } from './productlist/productlist.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { DeliveryComponent } from './delivery/delivery.component';
import { TrackorderComponent } from './trackorder/trackorder.component';
import { CustomerComponent } from './customer/customer.component';
import { StaffComponent } from './staff/staff.component';
import { AdminComponent } from './admin/admin.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FeedbacklistComponent } from './feedbacklist/feedbacklist.component';
import { OrderlistComponent } from './orderlist/orderlist.component';
import { StafflistComponent } from './stafflist/stafflist.component';
import { EditstaffComponent } from './editstaff/editstaff.component';
import { EditproductComponent } from './editproduct/editproduct.component';
import { StaffemailComponent } from './staffemail/staffemail.component';
import { CustomerlistComponent } from './customerlist/customerlist.component';
import { ProfileComponent } from './profile/profile.component';
import { CustomerprofileComponent } from './customerprofile/customerprofile.component';
import { CompletedordersComponent } from './completedorders/completedorders.component';
import { PendingordersComponent } from './pendingorders/pendingorders.component';
import { UserService } from './user.service';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactComponent } from './contact/contact.component';

const routes: Routes = [
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},


  {path:'home',component:HomeComponent, canActivate : [UserService]},
  {path:'admin',component:AdminComponent, canActivate : [UserService]},
  {path:'staff',component:StaffComponent, canActivate : [UserService]},
  {path:'customer',component:CustomerComponent, canActivate : [UserService]},
  {path:'trackorder',component:TrackorderComponent, canActivate : [UserService]},
  {path:'delivery',component:DeliveryComponent, canActivate : [UserService]},
  {path:'feedback',component:FeedbackComponent, canActivate : [UserService]},
  {path:'feedbacklist',component:FeedbacklistComponent, canActivate : [UserService]},
  {path:'orderlist',component:OrderlistComponent, canActivate : [UserService]},
  {path:'stafflist',component:StafflistComponent, canActivate : [UserService]},
  {path:'editstaff',component:EditstaffComponent, canActivate : [UserService]},
  {path:'productlist',component:ProductlistComponent, canActivate : [UserService]},
  {path:'editproduct',component:EditproductComponent, canActivate : [UserService]},
   {path:'staffemail',component:StaffemailComponent, canActivate : [UserService]},
   {path:'customerlist',component:CustomerlistComponent, canActivate : [UserService]},
   {path:'profile',component:ProfileComponent, canActivate : [UserService]},
   {path:'customerprofile',component:CustomerprofileComponent, canActivate : [UserService]},
   {path:'completedorders',component:CompletedordersComponent, canActivate : [UserService]},
   {path:'pendingorders',component:PendingordersComponent, canActivate : [UserService]},
   {path:'aboutus',component:AboutusComponent, canActivate : [UserService]},
   {path:'contact',component:ContactComponent, canActivate : [UserService]}

   
 
  

  


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
