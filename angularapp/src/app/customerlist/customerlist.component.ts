import { Component, OnInit } from '@angular/core';
import { UserService } from './../user.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customerlist',
  templateUrl: './customerlist.component.html',
  styleUrls: ['./customerlist.component.css']
})
export class CustomerlistComponent implements OnInit {

  customers =[]

  constructor(private router: Router,private userService:UserService) { }

  ngOnInit(): void {
    this.loadCustomers()
  }

  loadCustomers(){

    this.userService.getCustomerlist().subscribe(response=>{
      if (response['status'] == 'success') {
        this.customers = response['data']
        console.log(this.customers)
      } else {
        console.log(response['error'])
      }



    })
  }

}
