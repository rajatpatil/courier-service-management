import { Component, OnInit } from '@angular/core';
import { OrderService } from '../order.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-pendingorders',
  templateUrl: './pendingorders.component.html',
  styleUrls: ['./pendingorders.component.css']
})
export class PendingordersComponent implements OnInit {

  orders =[]
  count = 0

  constructor(private router: Router,
    private orderService:OrderService) { }

  ngOnInit(): void {

    this.loadPendingOrders()
  }

  loadPendingOrders(){


    this.orderService.getPendingOrders().subscribe(response =>{
      if (response['status'] == 'success') {
        this.orders = response['data']
        console.log(this.orders)

        this.count = this.orders.length
      } else {
        console.log(response['error'])
      }



    })
  }

}
