import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  color =''
  constructor(private router: Router) { }

  changeBackGroundColor(){

    this.color = prompt("enter the color name")
    document.body.style.backgroundColor=this.color
    

    

  }


  logout(){
    sessionStorage.clear()
    this.router.navigate(['/login'])



  }
}
