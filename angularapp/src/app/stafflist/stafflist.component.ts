import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-stafflist',
  templateUrl: './stafflist.component.html',
  styleUrls: ['./stafflist.component.css']
})
export class StafflistComponent implements OnInit {

  allstaff = ""
  constructor(private router:Router,
    private adminService:AdminService) { }

  ngOnInit(): void {
    this.getStaffList()
  }
  getStaffList(){
    this.adminService.getAllStaff().subscribe(response => {
      this.allstaff=response["data"]
      console.log(this.allstaff)
    })
  }
  updateStaff(staff){
    this.router.navigate(['/editstaff'], {queryParams: {id: staff['staffId']}})
  }
  deleteStaff(staff){
    var temp = window.confirm("Are you sure ? Delet " + staff.staffName)
    if(temp == true)
    this.adminService.deleteStaff(staff.staffId).subscribe(response => {
      if (response["status"]) {
        window.alert("Deleted")
        this.getStaffList()
      }
    })
  }
}
