import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrderService } from '../order.service';

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.css']
})
export class DeliveryComponent implements OnInit {

 
  cid =0
  pid=0
 
  Orderdate =null
  pickupname=""
  pickupaddress =""
  pickupcity=""
  pickuppin=""
  pickupphone=""
  deliveryname =""
  deliveryaddress=""
  deliverycity=""
  deliverypin=""
  deliveryphone=""

  

  constructor(private router: Router,
    private orderService:OrderService) { }

  ngOnInit(): void {
  }

  giveOrder(){

    this.orderService.giveOrder(this.cid,this.pid,this.Orderdate,this.pickupname,this.pickupaddress,this.pickupcity,this.pickuppin,this.pickupphone,
      this.deliveryname,this.deliveryaddress,this.deliverycity,this.deliverypin,this.deliveryphone).subscribe(response =>{
        if (response['status'] == 'success') {
          console.log("data added")
          this.router.navigate(['/customer'])
       
      }

      })

  }

}
