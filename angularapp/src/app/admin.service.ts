import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  url = 'http://localhost:4000/admin'
  constructor(private httpClient:HttpClient) { }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
        // client-side error
        errorMessage = `Error: ${error.error.message}`;
        alert(errorMessage);
        
    } else {
        // server-side error
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        alert(errorMessage);
       
    }
    console.log(errorMessage);
    return throwError(errorMessage);
}

  getAllStaff(){
    return this.httpClient.get(this.url + '/getAllStaff')
  }
  getStaffDetails(id){
    return this.httpClient.get(this.url + '/getStaffById/' + id)
  }
  getProductDetails(id){
    return this.httpClient.get(this.url + '/getProductByProductId/' + id)
  }
  deleteStaff(id){
    return this.httpClient.delete(this.url + '/deleteStaff/'+ id)
  }
  deleteProduct(id){
    return this.httpClient.delete(this.url + '/deleteProduct/'+ id)
  }
  updateStaff(id,name:string,phone:string,email:string){
    const body = {
      staffName : name,
      staffPhone : phone,
      staffEmail : email
    }
    return this.httpClient.put(this.url + '/updateStaff/'+ id, body)
  }
  updateProduct(id,name:string,rate:number){
    const body = {
      productName : name,
      productRate : rate
    }
    return this.httpClient.put(this.url + '/updateProduct/'+ id, body)
  }
  addStaff(password:string,name:string,phone:string,email:string){
    const body = {
      password : password,
      staffName : name,
      staffPhone : phone,
      staffEmail : email
    }
    return this.httpClient.post(this.url + '/addStaff', body)
  }
  getAllProducts(){
    return this.httpClient.get(this.url + '/getAllProducts')
  }
  addProduct(name:string,rate:number,){
    const body = {
      productName : name,
      productRate : rate
    }
    return this.httpClient.post(this.url + '/createProduct', body)
  }
}
