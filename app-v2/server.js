const express = require('express')
const bodyParser = require('body-parser')


// get the routers
const routerCustomer = require('./routes/customer')
const routerProduct = require('./routes/product')
const routerAdmin = require('./routes/admin')


const app = express()
app.use(bodyParser.json())

// add the routers
app.use('/customer', routerCustomer)
app.use('/product', routerProduct)
app.use('/admin', routerAdmin)

// default handler
app.get('/',(request,response) =>{
    response.send("welcome to my courier service")
})

app.listen(3000,'0.0.0.0',()=>{
    console.log("server started listening at port no 3000") 
})