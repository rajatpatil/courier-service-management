const express = require('express')
const db = require('../db')
const utils = require('../utils')
const config = require('../config')
const { request } = require('express')
const router = express.Router()


// GET
/**
 * @swagger
 *
 * /order:
 *   get:
 *     description: For getting list of orders for Admin
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: successful message
 */



router.get('/getorderdetails', (request, response) => {
  const {oid}=request.body
const statement = `select od.orderId,customerId,productId,orderDate,
 deliveryDate,orderStatus,pickupName,pickupAddress,deliveryName,
 deliveryAddress,pickupPin,deliveryPin,pickupPhone,deliveryPhone from order_details od ,orders o where od.orderId=o.orderId `
db.query(statement,(error,data)=>
{
  response.send(data)
})

})
//Delete
/**
 * @swagger
 *
 * /order/:id:
 *   delete:
 *     description: Deleting order
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: orderId
 *         description: id of order
 *         in: formData
 *         required: true
 *         type: number 
 *     responses:
 *       200:
 *         description: successful message
 */


router.delete('/removeorder', (request, response) => {
  const {id} = request.body
  const statement1 = `delete from order_details where orderId =${id};delete from orders where orderId =${id}`


  db.query(statement1,(error, data) => {
    response.send(utils.createResult(error, data))
 

  
  })
 
})
router.put('/updateorderstatus', (request, response) => {
  const {id} = request.body
  const statement1 = `update order_details set orderStatus = 1 where orderId =${id}`


  db.query(statement1,(error, data) => {
    response.send(utils.createResult(error, data))
 

  
  })
 
})






module.exports = router