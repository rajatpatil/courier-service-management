const express = require('express')
const db = require('../db')
const utils = require('../utils')
const bodyParser = require('body-parser')
const app = express()
app.use(bodyParser.json())

app.get('/', (request, response) => {
    const statement = `select * from order_details`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

app.post('/', (request, response) => {
    const {orderId,staffId,orderDate,deliveryDate,orderStatus,
        pickupName,pickupAddress,pickupCity,pickupPin,pickupPhone,
        deliveryName,deliveryAddress,deliveryCity,deliveryPin,deliveryPhone} = request.body
    const statement = `insert into order_details (orderId,staffId,orderDate,deliveryDate,orderStatus,
        pickupName,pickupAddress,pickupCity,pickupPin,pickupPhone,
        deliveryName,deliveryAddress,deliveryCity,deliveryPin,deliveryPhone) values (${orderId},${staffId},'${orderDate}','${deliveryDate}',${orderStatus},
            '${pickupName}','${pickupAddress}','${pickupCity}','${pickupPin}','${pickupPhone}',
            '${deliveryName}','${deliveryAddress}','${deliveryCity}','${deliveryPin}','${deliveryPhone}')`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

app.put('/:orderId', (request, response) => {
  const {orderId} = request.params
  const {staffId,orderDate,deliveryDate,orderStatus,
    pickupName,pickupAddress,pickupCity,pickupPin,pickupPhone,
    deliveryName,deliveryAddress,deliveryCity,deliveryPin,deliveryPhone} = request.body
  
  
    const statement = `update order_details set staffId = ${staffId},orderDate = '${orderDate}',deliveryDate = '${deliveryDate}',
  orderStatus = ${orderStatus},pickupName = '${pickupName}',pickupAddress = '${pickupAddress}',pickupCity = '${pickupCity}',
  pickupPin = '${pickupPin}',pickupPhone ='${pickupPhone}',
  deliveryName = '${deliveryName}',deliveryAddress = '${deliveryAddress}',deliveryCity = '${deliveryCity}',
  deliveryPin = '${deliveryPin}',deliveryPhone = '${deliveryPhone}' 
  where orderId = ${orderId}`
  
  
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})
app.delete('/:orderId', (request, response) => {
    const {orderId} = request.params
    const statement = `delete from order_details where orderId = ${orderId}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })
app.listen(3000, '0.0.0.0', () => {
    console.log('server started on port 3000')
  })