const express = require('express')
const db = require('../db')
const utils = require('../utils')
const crypto = require('crypto-js')

router = express.Router()



router.get('/:customerId', (request, response) => {

       const {customerId} =  request.params

    const statement = `select customerName, customerEmail, customerPhone from customers where customerId = ${customerId}`
    
    db.query(statement, (error, users) => {
        if (error) {
            response.send({ status: 'error', error: error })
        } else {
            if (users.length == 0) {
                // user does not exist
                response.send({ status: 'error', error: 'user does not exist' })
            } else {
                // user exists
                const user = users[0]
                response.send({ status: 'success', data: user })
            }
        }
    })
})



router.post('/signup', (request, response) => {
    const { customerName, customerPhone, customerEmail, password } = request.body
    // encrypt user's password
    const ecryptedPassword = crypto.SHA256(password)

    const statement = `insert into customers (customerName, customerPhone, customerEmail, password) values 
        ('${customerName}', '${customerPhone}', '${customerEmail}', '${ecryptedPassword}')`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})



router.post('/signin', (request, response) => {
    const { customerEmail, password } = request.body

    const encryptedPassword = crypto.SHA256(password)
    const statement = `select customerName,customerEmail,customerPhone from customers where customerEmail = '${customerEmail}' and password = '${encryptedPassword}'`
    db.query(statement, (error, customers) => {
        if (error) {
            response.send({ status: 'error', error: error })
        } else {
            if (customers.length == 0) {
                // user does not exist
                response.send({ status: 'error', error: 'customer does not exist' })
            } else {
                // user exists
                const customer = customers[0]
                response.send({
                    status: 'success', data: {
                        customerName: customer['customerName'],
                        customerEmail: customer['customerEmail'],
                        customerPhone: customer['customerPhone'],
                    }
                })
            }
        }
    })
})


router.put('/', (request, response) => {
    const { customerName, customerPhone, customerEmail} = request.body
    
    const statement = `update customers set customerName = '${customerName}', customerPhone = '${customerPhone}' where customerEmail = '${customerEmail}'`
    
    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            response.send(utils.createSuccess(data))
            
        }
    })
  })
  
  router.delete('/', (request, response) => {
   
    const { customerEmail} = request.body
    
    const statement = `delete from customers where customerEmail = '${customerEmail}'`
    
    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            response.send(utils.createSuccess(data))
            
        }
    })
  })
  




module.exports = router