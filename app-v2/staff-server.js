const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const jwt = require('jsonwebtoken')
const config = require('./config')
// swagger: for api documentation
const swaggerJSDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')


// routers
const staffRouter = require('./routes/staff')

const feedbackRouter = require('./routes/feedback')
const orderRouter = require('./routes/Sorder')


const app = express()
app.use(bodyParser.json())
app.use(morgan('combined'))
// swagger init

const swaggerOptions = {
  definition: {
    info: {
      title: 'Amazon Server',
      version: '1.0.0',
      description: 'This is a Express server for amazon application'
    }
  },
  apis: ['./admin/routes/*.js']
}

const swaggerSpec = swaggerJSDoc(swaggerOptions)
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));







// add a middleware for getting the id from token
function getUserId(request, response, next) {

  if (request.url == '/staff/signin' || request.url == '/staff/signup'
  ) {
    // do not check for token 
    next()
  } else {

    try {
      const token = request.headers['token']
      const data = jwt.verify(token, config.secret)

      // add a new key named userId with logged in user's id
      request.userId = data['id']

      // go to the actual route
      next()
      
    } catch (ex) {
      response.status(401)
      response.send({status: 'error', error: 'protected api'})
    }
  }
}

app.use(getUserId)



// add the routes
app.use('/staff', staffRouter)
app.use('/order',orderRouter)



app.use('/feedback', feedbackRouter)

// default route
app.get('/', (request, response) => {
  response.send('welcome to my application')
})

app.listen(3000, '0.0.0.0', () => {
  console.log('server started on port 3000')
})


