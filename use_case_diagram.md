```plantuml
left to right direction

customer --> (Login as a Customer)
customer --> (Register as a Customer)
customer --> (Update Profile)
customer --> (Book Pickup)
customer --> (Track Order)
customer --> (Raise a Complain)

Admin --> (Login as a Admin)
Admin --> (Add/Remove Employee)
Admin --> (Update profile of employee)
Admin --> (Check daily activities)
Admin --> (Approve orders and assign delivery person)

Delivery_person -->(Login as a delivery person)
Delivery_person -->(check prifile)
Delivery_person -->(check pending orders)
Delivery_person -->(check completed orders)
Delivery_person -->(Pickup order)
Delivery_person -->(Deliver order)
Delivery_person -->(Update Status)

Support_Staff -->(Login as a support staff)
Support_Staff -->(Check Complaints)
Support_Staff -->(Solve all issues)
Support_Staff -->(Update complain status)
```
