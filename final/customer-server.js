const express = require('express')
require('express-async-errors');
const bodyParser = require('body-parser')
const cors = require('cors')


// get the routers
const routerCustomer = require('./routes/customer/customer')
//const routerProduct = require('./routes/product')
//const routerAdmin = require('./routes/admin')


const app = express()
app.use(bodyParser.json())
app.use(cors())

// add the routers
app.use('/customer', routerCustomer)
//app.use('/product', routerProduct)
//app.use('/admin', routerAdmin)

// default handler
app.get('/',(request,response) =>{
    response.send("welcome to my courier service")
})

app.use((req, res, next) => {
    req.status = 404;
    const error = new Error("Routes not found");
    next(error);
 });
 
 //Error Handler
 app.use((error, req, res, next) => {
    res.status(req.status || 500).send({
        message:error.message,
        stack: error.stack
    });
 });

app.listen(3000,'0.0.0.0',()=>{
    console.log("server started listening at port no 3000") 
})
