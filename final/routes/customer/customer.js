const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const crypto = require('crypto-js')
const config = require('../../config')
const mailer =require('../../mailer')
const uuid = require('uuid')

router = express.Router()



router.post('/', (request, response) => {
    const { customerId, customerName, customerEmail,comment,status} = request.body
    
    const statement = `insert into feedback (customerId, customerName ,customerEmail, comment,status) values 
    (${customerId}, '${customerName}' ,'${customerEmail}', '${comment}','${status}')`
    
    db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
        
    })
  })
  
  
  router.get('/profile/:id', (request, response) => {
  const { id } = request.params
  const statement = `select * from customers where customerId = ${id}`
  db.query(statement, (error, data) => {
  response.send(data)
    
  })
})




router.post('/add/:id', (request, response) => {
  const { id } = request.params
    const {productId,orderDate,pickupName,pickupAddress,pickupCity,pickupPin,pickupPhone,
        deliveryName,deliveryAddress,deliveryCity,deliveryPin,deliveryPhone} = request.body

    const statement = `insert into order_details (customerId,productId,orderDate,pickupName,pickupAddress,pickupCity,pickupPin,pickupPhone,
      deliveryName,deliveryAddress,deliveryCity,deliveryPin,deliveryPhone) values (${id},${productId},'${orderDate}',
          '${pickupName}','${pickupAddress}','${pickupCity}','${pickupPin}','${pickupPhone}',
          '${deliveryName}','${deliveryAddress}','${deliveryCity}','${deliveryPin}','${deliveryPhone}')`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })


  router.get('/:customerId', (request, response) => {

    const {customerId} =  request.params

const statement = `select orderId,orderDate,deliveryDate,orderStatus,deliveryName,
                   deliveryAddress,deliveryCity,deliveryPin,deliveryPhone from 
                   order_details where customerId = ${customerId}`

 db.query(statement, (error, orders) => {
     if (error) {
         response.send({ status: 'error', error: error })
     } else {
         if (orders.length == 0) {
             // order does not exist
             response.send({ status: 'error', error: 'order does not exist' })
         } else {
             // order exists
             response.send({ status: 'success', data: orders })
         }
     }
 })
})


router.get('/forgot-password/:email', (request, response) => {
    const {email} = request.params
    const statement = `select customerId, customerName, customerPhone from customers where customerEmail = '${email}'`
    db.query(statement, (error, customers) => {
      if (error) {
        response.send(utils.createError(error))
      } else if (customers.length == 0) {
        response.send(utils.createError('customer does not exist'))
      } else {
        const customer = customers[0]
        const otp = utils.generateOTP()
        const body = `Your otp = ${otp}` 
  
        mailer.sendEmail(email, 'Reset your password', body,  (error, info) => {
          response.send(
            utils.createResult(error, {
              otp: otp,
              email: email
            })
          )
        })
      }
    })
  })
  


router.post('/signup', (request, response) => {
    const { name, email, password,phone  } = request.body
    // encrypt user's password
    const ecryptedPassword = crypto.SHA256(password)

    const statement = `insert into customers (customerName,  customerEmail, password,customerPhone) values 
        ('${name}','${email}', '${ecryptedPassword}','${phone}')`

    db.query(statement, (error, data) => {

      //response.send(utils.createResult(error, data))
        mailer.sendEmail(email, 'Welcome to couriers', config.body,  (error, info) => {
          response.send(utils.createResult(error, data))
        })
    
      })
})


router.post('/signin', (request, response) => {
    const { email, password } = request.body

    const encryptedPassword = crypto.SHA256(password)
    const statement = `select customerName,customerEmail,customerPhone from customers where customerEmail = '${email}' and password = '${encryptedPassword}'`
    db.query(statement, (error, customers) => {
        if (error) {
            response.send({ status: 'error', error: error })
        } else {
            if (customers.length == 0) {
                // user does not exist
                response.send({ status: 'error', error: 'customer does not exist' })
            } else {
                // user exists
                const customer = customers[0]
                response.send({
                    status: 'success', data: {
                        customerName: customer['customerName'],
                        customerEmail: customer['customerEmail'],
                        customerPhone: customer['customerPhone'],
                    }
                })
            }
        }
    })
})


router.put('/', (request, response) => {
    const { customerName, customerPhone, customerEmail,password} = request.body
    const encryptedPassword = crypto.SHA256(password)


    const statement = `update customers set customerName = '${customerName}', customerPhone = '${customerPhone}' where customerEmail = '${customerEmail}' and password = '${encryptedPassword}'`
    
    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            response.send(utils.createSuccess(data))
            
        }
    })
  })
  
  router.delete('/', (request, response) => {
   
    const { customerEmail,password} = request.body
    const encryptedPassword = crypto.SHA256(password)

    const statement = `delete from customers where customerEmail = '${customerEmail}' and password = '${encryptedPassword}'`
    
    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            response.send(utils.createSuccess(data))
            
        }
    })
  })
  


module.exports = router
