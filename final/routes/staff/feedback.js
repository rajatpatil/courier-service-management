const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const config = require('../../config')
const mailer =require('../../mailer')
const { request } = require('express')

const router = express.Router()

// GET
/**
 * @swagger
 *
 * /review/:productId:
 *   get:
 *     description: get product review
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: id of Category
 *         in: formData
 *         required: true
 *         type: number 
 *     responses:
 *       200:
 *         description: successful message
 */



router.get('/:id', (request, response) => {
  const {id} = request.params
  const statement = `select * from feedback where customerId = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})


router.get('/', (request, response) => {
  
  const statement = `select * from feedback`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})










// DELETE
/**
 * @swagger
 *
 * /review/:id:
 *   delete:
 *     description: Deleting review of product by id
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: id of productReview
 *         in: formData
 *         required: true
 *         type: number 
 *     responses:
 *       200:
 *         description: successful message
 */
 
 
 router.post('/add', (request, response) => {
    const { customerId, customerName, customerEmail,comment,status} = request.body
    
    const statement = `insert into feedback (customerId, customerName ,customerEmail, comment,status) values 
    (${customerId}, '${customerName}' ,'${customerEmail}', '${comment}','${status}')`
    
    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            response.send(utils.createSuccess(data))
            
        }
    })
  })


router.delete('/:id', (request, response) => {
  const {id} = request.params
  const statement = `delete from feedback where customerId = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------
router.put('/updatestatus/:id', (request, response) => {
  const {id} = request.params
  const statement = `update feedback set status="solved" where customerId = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})
router.post('/sendmail', (request, response) => {
  const {name, email} = request.body
  
 
  let body =`<div>
  <p>Dear ${name},We have checked your issue and solved your problem.Hope it's working good.</p>
  <p>If you have any query please contact us.</p>
    <div>
  <br>
  <div>Best regards,</div>
    <div>Staff</div>  

  `

    mailer.sendEmail(email, 'Regarding with Order Issue', body,  (error, info) => {
      console.log(error)
      console.log(info)
      response.send("Email Successfully Sent")
     
    })

  })



module.exports = router
