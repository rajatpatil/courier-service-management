const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const config = require('../../config')
const { request } = require('express')
const router = express.Router()


// GET
/**
 * @swagger
 *
 * /order:
 *   get:
 *     description: For getting list of orders for Admin
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: successful message
 */



router.get('/getorderdetails/:id', (request, response) => {
  const { id } =request.params
const statement = `select orderId,customerId,productId,orderDate,
 deliveryDate,orderStatus,pickupName,pickupAddress,deliveryName,
 deliveryAddress,pickupPin,deliveryPin,pickupPhone,deliveryPhone from order_details where staffId = ${id}`
db.query(statement,(error,data)=>
{
  response.send(data)
})

})

router.get('/', (request, response) => {
 
const statement = `select * from order_details`
db.query(statement,(error,data)=>
{
  response.send(utils.createResult(error,data))
})

})
//Delete
/**
 * @swagger
 *
 * /order/:id:
 *   delete:
 *     description: Deleting order
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: orderId
 *         description: id of order
 *         in: formData
 *         required: true
 *         type: number 
 *     responses:
 *       200:
 *         description: successful message
 */


router.delete('/removeorder/:id', (request, response) => {
  const {id} = request.params
  const statement1 = `delete from order_details where orderId =${id}`


  db.query(statement1,(error, data) => {
    response.send(utils.createResult(error, data))
 

  
  })
 
})
router.put('/updateorderstatus/:id', (request, response) => {
  const {id} = request.params
  const statement1 = `update order_details set orderStatus = 1 where orderId =${id}`


  db.query(statement1,(error, data) => {
    response.send(utils.createResult(error, data))
 

  
  })
 
})






module.exports = router
